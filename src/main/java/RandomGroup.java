import java.util.ArrayList;
import java.util.List;

public class RandomGroup {

    public List<String>[] generate(int numberOfGroupsWanted, List<String> peoples) {

        if (numberOfGroupsWanted <= 0) {
            throw new IllegalArgumentException("Group count must be greater than 0.");
        }
        if (numberOfGroupsWanted > peoples.size()) {
            throw new IllegalArgumentException("Group count should be lower than people count");
        }
        if (peoples.size() == 0) {
            throw new IllegalArgumentException("You are alone, looser!");
        }

        List<String>[] group = new ArrayList[numberOfGroupsWanted];

        while (!peoples.isEmpty()) {
            for (int j = 0; j < numberOfGroupsWanted && !peoples.isEmpty(); j++) {
                if (group[j] == null) {
                    group[j] = new ArrayList<>();
                }
                group[j].add((peoples.remove(0)));
            }
        }

        return group;
    }
}





