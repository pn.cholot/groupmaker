import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        File file = new File("./members.txt");
        String memberss = null;
        int numberOfGroupsWanted = 4;

        try (FileInputStream fis = new FileInputStream(file)) {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fis))) {

                List people = new ArrayList();

                while ((memberss = bufferedReader.readLine()) != null) {
                    people.add(memberss);
                }
                // TODO put this in group maker
                Collections.shuffle(people);

                // TODO call group maker !
                RandomGroup groupMaker = new RandomGroup();
                List[] groups = groupMaker.generate(numberOfGroupsWanted, people);


                // TODO return/print result
                //Arrays.stream(groups).forEach(System.out::println);

                for (int i = 0; i < numberOfGroupsWanted; i++) {
                    System.out.println(groups[i]);
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
