import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class RandomGroupTest {

    @Test
    public void valuesInitialValidTest1() {
        int numberOfGroupsWanted = 0;
        RandomGroup generator = new RandomGroup();

        List peoples = List.of("A", "B");

        boolean exceptionThrown = false;

        try {
            generator.generate(numberOfGroupsWanted, peoples);
        } catch (IllegalArgumentException ex) {
            exceptionThrown = true;
        }
        assertTrue("We should have an exception here, because we tried an invalid group count: 0.", exceptionThrown);

    }

    @Test
    public void valuesInitialValidTest2() {
        List peoples = List.of();
        int numberOfGroupsWanted = 0;
        boolean exceptionThrown = false;
        RandomGroup generator = new RandomGroup();

        try {
            generator.generate(numberOfGroupsWanted, peoples);
        } catch (IllegalArgumentException ex) {
            exceptionThrown = true;
        }
        assertTrue("We should have an exception here, because we tried an invalid group count: 3 vs people count: 2.", exceptionThrown);
    }

    @Test
    public void valuesInitialValidTest3() {
        int numberOfGroupsWanted = 5;
        RandomGroup generator = new RandomGroup();

        List<String> peoples = List.of("");

        boolean exceptionThrown = false;

        try {
            generator.generate(numberOfGroupsWanted, peoples);
        } catch (IllegalArgumentException ex) {
            exceptionThrown = true;
        }
        assertTrue("We should have at least one people", exceptionThrown);
    }


    // test si N° groupe > N° participants OOOOOOOOOOOOO

    // test si N° groupe < 0 OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

    // test si N° groupe = 0 OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

    // test si N° participats = 0 OOOOOOOOOOOOOOOOOOOOOOOOOO


    // test si N° participants uniformément réparti

    // test si nombre de groupe != nombre de groupe demandé OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

}

